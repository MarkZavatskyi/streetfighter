import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImg = createFighterImage(fighter);
  fighterImg.classList.add('mini');

  const details = createFighterDetails(fighter, [
    'name', 'health', 'attack', 'defense'
  ]);

  fighterElement.append(fighterImg, details);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterDetails(fighter, specifications = []) {
  const fighterDetails = createElement({
    tagName: 'ul',
    className: 'fighter-preview__details'
  });

  if (specifications.length === 0) {
    return fighterDetails;
  }

  specifications.forEach(key => {
    const specification = createElement({ tagName: 'li', className: key });
    specification.innerHTML = `<span>${key}:</span><span>${fighter[key]}</span>`;
    fighterDetails.append(specification);
  })

  return fighterDetails;
}
