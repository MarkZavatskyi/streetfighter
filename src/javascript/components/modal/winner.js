import { showModal } from "./modal";
import { createElement } from '../../helpers/domHelper';
import App from "../../app";
// import { createFightersSelector } from "../fighterSelector";

export function showWinnerModal(fighter) {
  const bodyElement = createElement({ tagName: 'div', className: 'winner_modal_body' });
  bodyElement.innerHTML = `<h1>${fighter.name } win!</h1>`;

  showModal({
    title: `The fight is over!`,
    bodyElement: bodyElement,
    onClose: () => {
      return new App();
    }
  });
}
