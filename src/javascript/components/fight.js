import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  let firstHp = firstFighter.health;
  let secondHp = secondFighter.health;

  let firstBlock = false;
  let secondBlock = false;

  let codes = new Set();
  let firstCritCooldown = false;
  let secondCritCooldown = false;

  window.addEventListener('keydown', (e) => {
    switch (e.code) {
      case controls.PlayerOneAttack:
        if (!secondBlock && !firstBlock) {
          secondHp -= getDamage(firstFighter, secondFighter);
          changeFighterHpIndicator('right', secondFighter.health, secondHp);
        }
        break;
      case controls.PlayerTwoAttack:
        if (!firstBlock && !secondBlock) {
          firstHp -= getDamage(secondFighter, firstFighter);
          changeFighterHpIndicator('left', firstFighter.health, firstHp);
        }
        break;
      case controls.PlayerOneBlock:
        firstBlock = true;
        break;
      case controls.PlayerTwoBlock:
        secondBlock = true;
        break;
    }
  });

  window.addEventListener('keyup', (e) => {
    switch (e.code) {
      case controls.PlayerOneBlock:
        firstBlock = false;
        break;
      case controls.PlayerTwoBlock:
        secondBlock = false;
        break;
    }
  });

  window.addEventListener('keydown', (e) => {
    codes.add(e.code);

    for (let code of controls.PlayerOneCriticalHitCombination) {
      if(!codes.has(code)) {
        return;
      }
    }

    codes.clear();

    if (!firstCritCooldown) {
      secondHp -= 2 * firstFighter.attack;
      changeFighterHpIndicator('right', secondFighter.health, secondHp);

      firstCritCooldown = true;
      setTimeout(() => { firstCritCooldown = false }, 10000);
    }
  });

  window.addEventListener('keydown', (e) => {
    codes.add(e.code);

    for (let code of controls.PlayerTwoCriticalHitCombination) {
      if(!codes.has(code)) {
        return;
      }
    }

    codes.clear();

    if (!secondCritCooldown) {
      firstHp -= 2 * secondFighter.attack;
      changeFighterHpIndicator('left', firstFighter.health, firstHp);

      secondCritCooldown = true;
      setTimeout(() => { secondCritCooldown = false }, 10000);
    }
  });

  document.addEventListener('keyup', (e) => {
    codes.delete(e.code);
  });

  return new Promise((resolve) => {
    setInterval(() => {
      if (firstHp < 0) {
        resolve(secondFighter);
      }

      if (secondHp < 0) {
        resolve(firstFighter);
      }
    }, 500);
  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);

  if (damage < 0) {
    damage = 0;
  }

  return damage
}

export function getHitPower(fighter) {
  const min = 1;
  const max = 2;
  const criticalHitChance = Math.random() + (max - min);

  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const min = 1;
  const max = 2;
  const dodgeChance = Math.random() + (max - min);

  return fighter.defense * dodgeChance;
}

export function changeFighterHpIndicator(side, defaultHp, hpNow) {
  const indicator = document.getElementById(`${side}-fighter-indicator`);
  if (hpNow > 0) {
    indicator.style.width = `${hpNow / (defaultHp / 100)}%`;
  } else {
    indicator.style.width = 0;
  }
}
